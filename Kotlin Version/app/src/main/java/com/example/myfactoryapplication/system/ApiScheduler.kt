package com.example.myfactoryapplication.system

import com.example.myfactoryapplication.MyFactoryApplication
import com.example.myfactoryapplication.presenters.DashboardPresenter
import kotlinx.coroutines.*

class ApiScheduler {
    private var job: Job? = null

    companion object {
        lateinit var presenter: DashboardPresenter
        private val scope = MainScope()
        const val fetchDataTimer: Long = 5
        const val timeFormatter: Long = 60000
    }

    fun startUpdates() {
        val timeDiff = System.currentTimeMillis() - StorageUtil(MyFactoryApplication.ApplicationContext).getLastFetchTime()

        stopUpdates()
        if (timeDiff > (fetchDataTimer * timeFormatter) || StorageUtil(MyFactoryApplication.ApplicationContext).loadNewsLocally().isEmpty()) {
            startJob(timeFormatter * fetchDataTimer, true)
        } else {
            startJob((timeFormatter * fetchDataTimer) - timeDiff, false)
        }
    }

    private fun startJob(timeMillis: Long, fetchData: Boolean) {
        job = scope.launch {
            while (isActive) {
                if (fetchData) {
                    presenter.getData()
                    delay(timeMillis)
                } else {
                    presenter.view.showResult(StorageUtil(MyFactoryApplication.ApplicationContext).loadNewsLocally())
                    delay(timeMillis)
                    startUpdates()
                }
            }
        }
    }

    fun stopUpdates() {
        job?.cancelChildren()
        scope.coroutineContext.cancelChildren()
        job?.cancel()
        job = null
    }
}