package com.example.myfactoryapplication.system

import android.content.Context
import android.content.SharedPreferences
import com.example.myfactoryapplication.models.obj.News
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type


class StorageUtil(context: Context) {
    private val PREFERENCES_FILE_NAME = "News Storage"
    private val PREFERENCES_LIST_NAME = "News List"
    private val PREFERENCES_DATE_NAME = "News CreatedAt"

    private val sharedPreferences: SharedPreferences = context.getSharedPreferences(
        PREFERENCES_FILE_NAME,
        Context.MODE_PRIVATE
    )
    private var editor: SharedPreferences.Editor? = null

    fun saveNewsLocally(newsList: ArrayList<News>) {
        editor = sharedPreferences.edit()
        val json = Gson().toJson(newsList)

        editor?.putString(PREFERENCES_LIST_NAME, json)
        editor?.putLong(PREFERENCES_DATE_NAME, System.currentTimeMillis())
        editor?.apply()
    }

    fun loadNewsLocally(): ArrayList<News> {
        var newsList: ArrayList<News>

        val json = sharedPreferences.getString(PREFERENCES_LIST_NAME, arrayListOf<News>().toString())
        val type: Type = object : TypeToken<ArrayList<News?>?>() {}.type
        newsList = Gson().fromJson(json, type)

        if (newsList == null) {
            newsList = ArrayList()
        }

        return newsList
    }

    fun getLastFetchTime(): Long {
        return sharedPreferences.getLong(PREFERENCES_DATE_NAME, System.currentTimeMillis())
    }

    fun setLastFetchTime() {
        editor = sharedPreferences.edit()
        editor?.putLong(PREFERENCES_DATE_NAME, System.currentTimeMillis())
        editor?.apply()
    }
}