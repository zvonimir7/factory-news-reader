package com.example.myfactoryapplication.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.myfactoryapplication.R
import com.example.myfactoryapplication.models.obj.News
import com.example.myfactoryapplication.ui.news_single.NewsActivity
import com.squareup.picasso.Picasso

class NewsAdapter(private val itemList: ArrayList<News>, private val context: Context) : RecyclerView.Adapter<NewsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_news, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = itemList[position]

        holder.title.text = data.title
        holder.description.text = data.description
        Picasso.get().load(data.urlToImage).into(holder.img)

        holder.layout.setOnClickListener {
            val intent = Intent(context, NewsActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("newsTitle", data.title)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    class ViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val title: TextView = item.findViewById(R.id.news_title)
        val description: TextView = item.findViewById(R.id.news_desc)
        val img: ImageView = item.findViewById(R.id.news_img)
        val layout: ConstraintLayout = item.findViewById(R.id.news_layout)
    }
}