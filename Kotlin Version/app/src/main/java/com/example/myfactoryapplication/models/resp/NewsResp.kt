package com.example.myfactoryapplication.models.resp

import com.example.myfactoryapplication.models.obj.News
import com.google.gson.annotations.SerializedName

data class NewsResp(
    @SerializedName("articles")
    val newsList: ArrayList<News> = arrayListOf()
)