package com.example.myfactoryapplication

import android.app.Application
import android.content.Context

class MyFactoryApplication : Application() {
    companion object {
        lateinit var ApplicationContext: Context
            private set
    }

    override fun onCreate() {
        super.onCreate()
        ApplicationContext = this
    }
}
