package com.example.myfactoryapplication.ui.news_single

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.example.myfactoryapplication.R
import com.example.myfactoryapplication.adapters.NewsFragmentAdapter
import com.example.myfactoryapplication.databinding.ActivityNewsBinding
import com.example.myfactoryapplication.system.StorageUtil

class NewsActivity : AppCompatActivity() {
    private lateinit var activityNewsBinding: ActivityNewsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityNewsBinding = ActivityNewsBinding.inflate(layoutInflater)
        setContentView(activityNewsBinding.root)

        setupToolbar()
        setupUI()
    }

    private fun setupUI() {
        val dataNews = StorageUtil(this).loadNewsLocally()

        if (dataNews.isEmpty()) {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Greška")
            builder.setMessage("Ups došlo je do pogreške.")

            builder.setNegativeButton("U REDU") { _, _ -> }
            builder.show()

            return
        }

        val adapter = NewsFragmentAdapter(this)
        var currentNews = -1
        for ((index, news) in dataNews.withIndex()) {
            val myFragment: NewsFragment = NewsFragment.newInstance(news.title, news.description, news.urlToImage)
            adapter.addFragment(myFragment)

            if (intent.getStringExtra("newsTitle") == news.title) {
                currentNews = index
            }
        }

        val viewPager2: ViewPager2 = activityNewsBinding.newsViewPager2
        viewPager2.adapter = adapter

        viewPager2.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                activityNewsBinding.newsToolbar.title = dataNews[position].title

                super.onPageSelected(position)
            }
        })

        if (currentNews >= 0) {
            viewPager2.currentItem = currentNews
        }
    }

    private fun setupToolbar() {
        activityNewsBinding.newsToolbar.title = intent.getStringExtra("newsTitle")
        activityNewsBinding.newsToolbar.setNavigationIcon(R.drawable.back_icon)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        setSupportActionBar(activityNewsBinding.newsToolbar)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        finish()

        return super.onSupportNavigateUp()
    }
}