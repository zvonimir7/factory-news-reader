package com.example.myfactoryapplication.ui.news_single

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.myfactoryapplication.databinding.FragmentNewsBinding
import com.squareup.picasso.Picasso

class NewsFragment : Fragment() {
    private lateinit var newsBinding: FragmentNewsBinding

    companion object {
        private const val FRAG_TITLE = "Unknown Title"
        private const val FRAG_DESC = "Unknown Description"
        private const val FRAG_IMG_URL = "https://gepig.com/game_cover_460w/4303.jpg"

        fun newInstance(title: String, desc: String, img_url: String) = NewsFragment().apply {
            arguments = Bundle(2).apply {
                putString(FRAG_TITLE, title)
                putString(FRAG_DESC, desc)
                putString(FRAG_IMG_URL, img_url)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        newsBinding = FragmentNewsBinding.inflate(inflater, container, false)

        setupUI()
        return newsBinding.root
    }

    private fun setupUI() {
        newsBinding.newsFragmentTitle.text = arguments?.getString(FRAG_TITLE) ?: FRAG_TITLE
        newsBinding.newsFragmentDesc.text = arguments?.getString(FRAG_DESC) ?: FRAG_DESC

        val imgUrl = arguments?.getString(FRAG_IMG_URL) ?: FRAG_IMG_URL
        Picasso.get().load(imgUrl).into(newsBinding.newsFragmentImg)
    }
}