package com.example.myfactoryapplication.ui.news_dashboard

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myfactoryapplication.adapters.NewsAdapter
import com.example.myfactoryapplication.databinding.ActivityDashboardBinding
import com.example.myfactoryapplication.models.obj.News
import com.example.myfactoryapplication.presenters.DashboardPresenter
import com.example.myfactoryapplication.system.ApiScheduler

class DashboardActivity : AppCompatActivity(), DashboardInterface {
    private lateinit var presenter: DashboardPresenter
    private lateinit var dashboardBinding: ActivityDashboardBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dashboardBinding = ActivityDashboardBinding.inflate(layoutInflater)
        setContentView(dashboardBinding.root)

        dashboardBinding.dashboardRecyclerView.adapter = NewsAdapter(ArrayList(), this)
        dashboardBinding.dashboardRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        dashboardBinding.dashboardToolbar.title = "Factory News"
        presenter = DashboardPresenter(this)
        ApiScheduler.presenter = presenter
    }

    override fun showLoading() {
        dashboardBinding.dashboardProgressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        dashboardBinding.dashboardProgressBar.visibility = View.GONE
    }

    override fun showError(msg: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Greška")
        builder.setMessage(msg)

        builder.setNegativeButton("U REDU") { _, _ -> }
        builder.show()
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun showResult(result: Any) {
        val data = result as ArrayList<News>
        val adapter = NewsAdapter(data, this)

        dashboardBinding.dashboardRecyclerView.adapter = adapter
        (dashboardBinding.dashboardRecyclerView.adapter as NewsAdapter).notifyDataSetChanged()
    }


    override fun onResume() {
        ApiScheduler().startUpdates()

        super.onResume()
    }

    override fun onPause() {
        ApiScheduler().stopUpdates()

        super.onPause()
    }
}