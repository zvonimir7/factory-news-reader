package com.example.myfactoryapplication.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class NewsList {
    @SerializedName("articles")
    private ArrayList<News> newsList;

    public ArrayList<News> getNewsArrayList() {
        return newsList;
    }

    public void setNewsArrayList(ArrayList<News> NewsArrayList) {
        this.newsList = NewsArrayList;
    }
}