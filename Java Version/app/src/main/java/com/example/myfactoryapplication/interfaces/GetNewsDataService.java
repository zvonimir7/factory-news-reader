package com.example.myfactoryapplication.interfaces;

import com.example.myfactoryapplication.models.NewsList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GetNewsDataService {
    @GET("articles?")
    Call<NewsList> getNewsData(@Query("source") String source, @Query("sortBy") String sortBy, @Query("apiKey") String apiKey);
}