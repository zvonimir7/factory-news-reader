package com.example.myfactoryapplication.helpers;

import android.app.AlertDialog;
import android.content.Context;

public class CustomAlerts {
    public static void showError(Context context, String type) {
        if (type.equals("network")) {
            //network error
        } else if (type.equals("database")) {
            AlertDialog alertDialog = new AlertDialog.Builder(context).create();
            alertDialog.setTitle("Greška");
            alertDialog.setMessage("Ups, pogreška prilikom dohvaćanja podataka, molimo kontaktirajte službu za korisnike.");
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "U REDU",
                    (dialog, which) -> dialog.dismiss());
            alertDialog.show();
        } else {
            AlertDialog alertDialog = new AlertDialog.Builder(context).create();
            alertDialog.setTitle("Greška");
            alertDialog.setMessage("Ups, došlo je do pogreške.");
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "U REDU",
                    (dialog, which) -> dialog.dismiss());
            alertDialog.show();
        }
    }
}
