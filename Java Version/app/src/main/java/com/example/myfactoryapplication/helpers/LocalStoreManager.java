package com.example.myfactoryapplication.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.myfactoryapplication.models.News;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class LocalStoreManager {
    public static void saveNewsLocally(Context context, ArrayList<News> newsList) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("shared preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();

        String json = gson.toJson(newsList);
        editor.putString("News list", json);
        editor.putLong("News createdAt", System.currentTimeMillis());
        editor.apply();
    }

    public static ArrayList<News> loadNewsLocally(Context context) {
        ArrayList<News> newsList;
        SharedPreferences sharedPreferences = context.getSharedPreferences("shared preferences", Context.MODE_PRIVATE);
        Gson gson = new Gson();

        String json = sharedPreferences.getString("News list", null);
        Type type = new TypeToken<ArrayList<News>>() {
        }.getType();
        newsList = gson.fromJson(json, type);
        if (newsList == null) {
            newsList = new ArrayList<>();
        }

        return newsList;
    }

    public static Long getLastFetchTime(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("shared preferences", Context.MODE_PRIVATE);
        return sharedPreferences.getLong("News createdAt", System.currentTimeMillis());
    }
}
