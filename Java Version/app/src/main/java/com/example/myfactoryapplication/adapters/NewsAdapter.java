package com.example.myfactoryapplication.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myfactoryapplication.R;
import com.example.myfactoryapplication.models.News;
import com.example.myfactoryapplication.ui.NewsActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> {
    private final ArrayList<News> dataList;

    public NewsAdapter(ArrayList<News> dataList) {
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.single_row_news, parent, false);
        return new NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position) {
        holder.tv_title.setText(dataList.get(position).getTitle());
        holder.tv_desc.setText(dataList.get(position).getDescription());
        Picasso.get().load(dataList.get(position).getUrlToImage()).into(holder.iv_img);

        holder.layout_news.setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), NewsActivity.class);
            intent.putExtra("newsTitle", dataList.get(position).getTitle());
            v.getContext().startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    static class NewsViewHolder extends RecyclerView.ViewHolder {
        TextView tv_title, tv_desc;
        ImageView iv_img;
        ConstraintLayout layout_news;

        NewsViewHolder(View itemView) {
            super(itemView);

            tv_title = itemView.findViewById(R.id.news_title);
            tv_desc = itemView.findViewById(R.id.news_desc);
            iv_img = itemView.findViewById(R.id.news_img);
            layout_news = itemView.findViewById(R.id.news_layout);
        }
    }
}
