package com.example.myfactoryapplication.ui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager2.widget.ViewPager2;

import com.example.myfactoryapplication.R;
import com.example.myfactoryapplication.adapters.NewsSliderAdapter;
import com.example.myfactoryapplication.fragments.NewsFragment;
import com.example.myfactoryapplication.helpers.CustomAlerts;
import com.example.myfactoryapplication.helpers.LocalStoreManager;
import com.example.myfactoryapplication.models.News;

import java.util.ArrayList;

public class NewsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        initializeUI();
    }

    private void initializeUI() {
        setupToolbar();

        NewsSliderAdapter newsSliderAdapter = new NewsSliderAdapter(this);
        ArrayList<News> news = LocalStoreManager.loadNewsLocally(this);

        if (news.isEmpty()) {
            CustomAlerts.showError(this, "unknown");
        }

        int currentNews = -1;
        for (int i = 0; i < news.size(); i++) {
            Bundle bundle = new Bundle();
            bundle.putString("title", news.get(i).getTitle());
            bundle.putString("desc", news.get(i).getDescription());
            bundle.putString("imgUrl", news.get(i).getUrlToImage());
            NewsFragment newFragment = NewsFragment.create(bundle);
            newsSliderAdapter.addFragment(newFragment);

            if (news.get(i).getTitle().equals(getIntent().getStringExtra("newsTitle"))) {
                currentNews = i;
            }
        }

        ViewPager2 viewPager2 = findViewById(R.id.news_viewPager2);
        viewPager2.setAdapter(newsSliderAdapter);

        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                Toolbar toolbar = findViewById(R.id.news_toolbar);
                toolbar.setTitle(news.get(position).getTitle());

                super.onPageSelected(position);
            }
        });

        if (currentNews >= 0)
            viewPager2.setCurrentItem(currentNews);
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.news_toolbar);
        toolbar.setTitle(getIntent().getStringExtra("newsTitle"));
        toolbar.setNavigationIcon(R.drawable.back_button);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        finish();

        return true;
    }
}